-- Answer a series of questions regarding the information provided

    -- List books Authored by Marjorie Green
        - "The Busy Executives Database Guide"
        - "You Can Combat Computer Stress"
    -- List the books Authored by Michael O'Leary
        - "Cooking with Computers"
    -- Write the author/s of "The Busy Executives Database Guide"
        - "Marjorie Green"
        - "Abraham Bennet"
    -- Identify the publisher of "But is it User Friendly"
        -"Algodata Infosystem"
    -- List the books published by Algodata Infosystem
        - "The Busy Executives Database Guide"
        - "Cooking with Computers"
        - "Straight Talk About Computers"
        - "But is it User Friendly"
        - "Secrets of Silicon Valley"
        - "Net Etiquette"
    

-- Create a database for a blog using SQL syntax

CREATE DATABASE blog_db;

USE blog_db;

--users
CREATE TABLE users(
id INT NOT NULL AUTO_INCREMENT,
email VARCHAR(100) NOT NULL,
password VARCHAR(300) NOT NULL,
datetime_create DATETIME NOT NULL,
PRIMARY KEY(id)
);

--posts
CREATE TABLE posts(
id INT NOT NULL AUTO_INCREMENT,
author_id INT NOT NULL,
title VARCHAR(500) NOT NULL,
content VARCHAR(5000) NOT NULL,
datetime_posted DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_author_id
    FOREIGN KEY(author_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

--comments
CREATE TABLE post_comments(
id INT NOT NULL AUTO_INCREMENT,
post_id INT NOT NULL,
user_id INT NOT NULL,
datetime_commented DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_post_id
    FOREIGN KEY(post_id) REFERENCES posts(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
CONSTRAINT fk_user_id
    FOREIGN KEY(user_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

--likes
CREATE TABLE post_likes(
id INT NOT NULL AUTO_INCREMENT,
post_id INT NOT NULL,
user_id INT NOT NULL,
datetime_liked DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_post_id2
    FOREIGN KEY(post_id) REFERENCES posts(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
CONSTRAINT fk_user_id2
    FOREIGN KEY(user_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);
